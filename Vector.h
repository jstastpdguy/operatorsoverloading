#pragma once
#include <iostream>

namespace homework{
	template<class T>
	class vector;
	
	template<class T>
	std::ostream& operator<< (std::ostream &, vector<T> const&);
	template<class T>
	std::istream& operator>> (std::istream &, vector<T> &);
	template <class T>
	vector<T> operator+ (vector<T> const&, vector<T> const&);
	template <class T>
	vector<T> operator- (vector<T> const&, vector<T> const&);
	template <class T>
	bool operator > (vector<T> const &, vector<T> const &);
	
	template<class T>
	class vector
	{
		T x;
		T y;
		T z;
	
	public:
	
		vector(): x(0), y(0), z(0) {};
		vector(T x, T y, T z) : x(x), y(y), z(z) {};

		operator float();
		vector<T> operator*(T) const;
		float operator[] (int) const;

		friend vector<T> operator + <T>(vector<T> const&, vector<T> const&);
		friend vector<T> operator - <T>(vector<T> const&, vector<T> const&);
		
		friend bool operator > <T>(vector<T> &, vector<T> &);

		friend std::ostream& operator << <T>(std::ostream &, vector<T> const&);
		friend std::istream& operator >> <T>(std::istream &, vector<T> &);
	};

}