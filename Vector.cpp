#include "vector.h"
#include <iostream>
#include <cmath>

namespace homework{

	template<typename T>
	std::ostream& operator << (std::ostream& os, vector<T> const& vector)
	{
		return	os << "{ " << vector.x << ", " << vector.y << ", " << vector.z << " }";
	}

	template<typename T>
	std::istream& operator >> (std::istream& is, vector<T> & vector)
	{
		is >> vector.x;
		is >> vector.y;
		is >> vector.z;

		return is;
	}

	template<class T>
	vector<T>::operator float()
	{
		return sqrt(x*x + y*y + z*z);
	}

	template<class T>
	vector<T> vector<T>::operator* (T value) const
	{
		return vector(x*value, y*value, x*value);
	}

	template<class T>
	vector<T> operator + (vector<T> const& a, vector<T> const& b)
	{
		return vector<T>(a.x + b.x, a.y + b.y, a.z + b.z);
	}

	template<class T>
	vector<T> operator - (vector<T> const& a, vector<T> const& b)
	{
		return vector<T>(a.x - b.x, a.y - b.y, a.z - b.z);
	}

	template<class T>
	bool operator > (vector<T> const & a, vector<T> const & b)
	{
		return static_cast<float>(const_cast<vector<T>>(a)) > static_cast<float>(const_cast<vector<T>>(b)) ? true : false;
	}

	template<class T>
	float vector<T>::operator[] (int index) const
	{
		if(index > -1 && index < 3)
		{
			switch(index)
			{
			case 0:
				return x;
				break;
			case 1:
				return y;
				break;
			case 2:
				return z;
				break;
			default:
				return 0;
			}
		}else std::cout << "vector operator [] invalid index";
		return 0;
	}

}	