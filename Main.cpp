#include <iostream>
#include "vector.cpp"

using namespace homework;

int main()
{
	vector<int> one(3, 3, 3), two;

	std::cout << "vector one: " << one << std::endl;
	
	std::cout << "\nenter values for vector({x, y, z}): \n";
	std::cin >> two;

	std::cout << "\nvector two: " << two << std::endl;

	std::cout << "\nvector one multiplied by 5: " << one * 5 << std::endl;

	std::cout << "\ngetting values of vector two by index: \n";

	for(int i = 0; i < 3; i++)
	{
		std::cout << i + 1 << " : " << two[i] << " | ";
	}

	std::cout << std::endl;

	std::cout << "\none + two : " << one + two << std::endl;
	std::cout << "one - two : " << one - two << std::endl;

	std::cout << "\none length: " << static_cast<float>(one) << std::endl
	<< "two length: " << static_cast<float>(two) << std::endl;

	one > two ? std::cout << "\none > two\n" : std::cout << "\none < two\n";
	return 0;
}